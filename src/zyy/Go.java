package zyy;

import java.util.Date;

public class Go {

  public static void main(String[] args) throws InterruptedException {
    Date now = new Date();
    FakeTime.init(now, 8, 10);
    System.out.printf(
      "\n上班时间是：%tT\n下班时间是：%tT\n",
      FakeTime.fake_start,
      FakeTime.fake_end
    );
  }
}
